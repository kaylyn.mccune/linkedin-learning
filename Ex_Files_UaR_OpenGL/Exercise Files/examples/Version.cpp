/*
 * =====================================================================================
 *
 *       Filename:  xVersion.cpp
 *
 *    Description: initialize a context and print out OpenGL version
 *
 *        Version:  1.0
 *        Created:  06/10/2014 12:39:02
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Pablo Colapinto (), gmail -> wolftype
 *   Organization:  
 *
 * =====================================================================================
 */

//Include OpenGL for graphics and Glfw for windowing
#include <stdlib.h>
#define GLEW_STATIC
#include <GL/glew.h>
#include "GLFW/glfw3.h"
#include <iostream>

using namespace std;

int main(int argc, char * argv[]){

    //create a pointer to a glfw window
    GLFWwindow * window;
    //try to initialize glfw, if it fails, close the program
    if (!glfwInit()) exit(EXIT_FAILURE);
    //create a window with specified height, width, and name
    window = glfwCreateWindow(1024,768,"glfw",NULL,NULL);
    //if window failed to get created, close the program
    if (!window) {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
  //make this window our current one  
  glfwMakeContextCurrent(window);
    
  //Get Version String
  const GLubyte * p = glGetString(GL_VERSION);
  cout << "OpenGL Version: " << p << endl;
  return 0;
}

//POINT OF THE LESSON
//need to intialize a window before you can make any opengl calls