# Install script for directory: C:/Users/Kaylyn/Documents/School/Int CG/LIL Repo/linkedin-learning/Ex_Files_UaR_OpenGL/Exercise Files/glfw

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "C:/Program Files (x86)/UaR_OpenGL")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE DIRECTORY FILES "C:/Users/Kaylyn/Documents/School/Int CG/LIL Repo/linkedin-learning/Ex_Files_UaR_OpenGL/Exercise Files/glfw/include/GLFW" FILES_MATCHING REGEX "/glfw3\\.h$" REGEX "/glfw3native\\.h$")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/cmake/glfw" TYPE FILE FILES
    "C:/Users/Kaylyn/Documents/School/Int CG/LIL Repo/linkedin-learning/Ex_Files_UaR_OpenGL/Exercise Files/visualstudio/glfw/src/glfwConfig.cmake"
    "C:/Users/Kaylyn/Documents/School/Int CG/LIL Repo/linkedin-learning/Ex_Files_UaR_OpenGL/Exercise Files/visualstudio/glfw/src/glfwConfigVersion.cmake"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("C:/Users/Kaylyn/Documents/School/Int CG/LIL Repo/linkedin-learning/Ex_Files_UaR_OpenGL/Exercise Files/visualstudio/glfw/src/cmake_install.cmake")
  include("C:/Users/Kaylyn/Documents/School/Int CG/LIL Repo/linkedin-learning/Ex_Files_UaR_OpenGL/Exercise Files/visualstudio/glfw/tests/cmake_install.cmake")

endif()

